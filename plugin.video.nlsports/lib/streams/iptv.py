from ..utils import bitly, xbmcutil
import xml.etree.ElementTree as ET
import urllib2
import re

sourceSite='http://www.pastebin.com/raw.php?i='
paste = 'hzmpm54G'

def addStreams():
    pBar = xbmcutil.createProgressBar('NL Sports', 'Laden van streams...')
    ua = bitly.getUserAgent()
    content = bitly.getPage(sourceSite + paste, sourceSite, ua)

    root = ET.fromstring(content)
    items = root.findall('item')
    percentage = 0
    percUpdate = 100 / len(items)
    for item in items:
        percentage = percentage + percUpdate
        xbmcutil.updateProgressBar(pBar, percentage, 'Bezig met laden...')
        title = item.find('title').text
        thumb = item.find('thumbnail').text
        link = item.find('link').text
        if item.find('tvgids') is not None :
            now = now_playing(item.find('tvgids').text)
            title = title + " | [COLOR gold][I]Nu: "+now+"[/I][/COLOR]"
        xbmcutil.addMenuItem(title, link, 'true', thumb)
    
    xbmcutil.updateProgressBar(pBar, 100,'Gereed!')
    xbmcutil.endOfList()

def now_playing(zender):
    try :
        req = urllib2.Request('http://tvgids.mobi/'+str(zender)+'-Programma-vandaag.html')
        response = urllib2.urlopen(req)
        data = response.read()
        response.close()
        h4re = re.compile("<h4>(.*)</h4>", re.DOTALL)
        h4 = h4re.search(data).group(1)
        print h4
        nowre = re.compile('<a style="color: #E30000; text-decoration: none;" href="http://tvgids.mobi/(.*).html">(.*)</a>')
        now = nowre.search(h4).group(2)
    except :
        now = "ONBEKEND"
    return now
