from ..utils import bitly, xbmcutil
from . import veetle, sopcast
import re
sourceSite='http://www.bvls2016.sc/'
	
def addStreams():
    pBar = xbmcutil.createProgressBar('NL Sports', 'Laden van streams...')

    i=1
    while i <= 10 :
        xbmcutil.updateProgressBar(pBar, (i*10), 'BVLS - Stream '+str(i))
        addStream('stream'+str(i),'BVLS - Stream '+str(i))
        i = i + 1

    xbmcutil.updateProgressBar(pBar, 100,'Gereed!')
    xbmcutil.endOfList()
    
def addStream(stream, display) :
    streamUrl = findStream(stream) 
    print 'Stream = '+streamUrl
    if streamUrl[-4:] == '.flv' :
        veetle.addChannel(display, streamUrl, 'stv')
    else :
        if bitly.getResponse(streamUrl) :
            color = 'green'
        else :
            streamUrl = ''
            color = 'red'
        xbmcutil.addMenuItem('[COLOR '+color+']'+display+'[/COLOR][I]'+streamUrl+'[/I]', streamUrl, 'true', 'bvls','bvls')


def findStream(page) :
    ua = bitly.getUserAgent()
    page1 = resolveIframe(sourceSite + '/' + page +'.html')
    print page1
    page2 = resolveIframe(page1)
    print page2
    pagecontent = bitly.getPage(page2, sourceSite, ua)
    b64coded = bitly.getBaseEncodedString(pagecontent)
    streamUrl = bitly.getStreamUrl(b64coded)
    return streamUrl
    
def resolveIframe(page) :
    try :
        if(page[:4] != 'http') :
            page = sourceSite + '/' + page
        userAgent = bitly.getUserAgent()
        pagecontent = bitly.getPage(page, sourceSite, userAgent)
        regIframe = re.compile('iframe(.*?)src="(.*?)" id="myfr"', re.DOTALL)
        iframesrc = regIframe.search(pagecontent).group(2)
        return iframesrc
    except :
        return page

